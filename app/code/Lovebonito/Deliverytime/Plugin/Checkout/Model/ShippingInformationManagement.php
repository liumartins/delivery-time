<?php

/**
 * Created by PhpStorm.
 * User: lindomar
 * Date: 26/06/18
 * Time: 5:23 PM
 */

namespace Lovebonito\Deliverytime\Plugin\Checkout\Model;
class ShippingInformationManagement
{
    protected $quoteRepository;
    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository
    ) {
        $this->quoteRepository = $quoteRepository;
    }
    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $extAttributes = $addressInformation->getExtensionAttributes();
        $deliveryTime = $extAttributes->getDeliveryTime();
        $deliveryWeekend = $extAttributes->getDeliveryWeekend();
        $quote = $this->quoteRepository->getActive($cartId);
        $quote->setDeliveryTime($deliveryTime);
        $quote->setDeliveryWeekend($deliveryWeekend);
    }
}