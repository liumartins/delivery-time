<?php
/**
 * Created by PhpStorm.
 * User: lindomar
 * Date: 24/06/18
 * Time: 2:48 PM
 */

\Magento\Framework\Component\ComponentRegistrar::register(
        \Magento\Framework\Component\ComponentRegistrar::MODULE,
        'Lovebonito_Deliverytime',
        __DIR__
    );
