<?php
/**
 * Created by PhpStorm.
 * User: lindomar
 * Date: 25/06/18
 * Time: 11:01 AM
 */


namespace Lovebonito\Deliverytime\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'delivery_time',
            [
                'type' => 'text',
                'nullable' => false,
                'comment' => 'Delivery Time',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'delivery_time',
            [
                'type' => 'text',
                'nullable' => false,
                'comment' => 'Delivery Time',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'delivery_time',
            [
                'type' => 'text',
                'nullable' => false,
                'comment' => 'Delivery Time',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'delivery_weekend',
            [
                'type' => 'text',
                'nullable' => false,
                'comment' => 'Delivery On Weekends',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'delivery_weekend',
            [
                'type' => 'text',
                'nullable' => false,
                'comment' => 'Delivery On Weekends',
            ]
        );
        


        $setup->endSetup();
    }
}