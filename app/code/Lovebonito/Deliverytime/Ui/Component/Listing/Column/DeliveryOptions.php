<?php

/**
 * Created by PhpStorm.
 * User: lindomar
 * Date: 29/06/18
 * Time: 1:26 PM
 */


namespace Lovebonito\Deliverytime\Ui\Component\Listing\Column;

class DeliveryOptions implements \Magento\Framework\Option\ArrayInterface
{
    //Here you can __construct Model

    public function toOptionArray()
    {
        // return your data
        return [
                    [
                        'value' => 'Anytime',
                        'label' => __('Anytime')
                    ],
                    [
                        'value' => 'Day',
                        'label' => __('Day')
                    ],
                    [
                        'value' => 'Night',
                        'label' => __('Night')

                    ]
        ];
    }
}