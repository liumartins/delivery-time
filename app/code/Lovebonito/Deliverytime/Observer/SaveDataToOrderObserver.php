<?php

/**
 * Created by PhpStorm.
 * User: lindomar
 * Date: 26/06/18
 * Time: 4:23 PM
 */
namespace Lovebonito\Deliverytime\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class SaveDataToOrderObserver implements ObserverInterface
{




    public function execute(EventObserver $observer)
    {
        $order = $observer->getOrder();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        $quote = $cart->getQuote();
        $order->setDeliveryTime($quote->getDeliveryTime());
        $order->setDeliveryWeekend($quote->getDeliveryWeekend());
        return $this;

    }
}