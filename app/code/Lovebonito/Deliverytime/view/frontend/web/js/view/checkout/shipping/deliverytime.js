/**
 * Created by lindomar on 24/06/18.
 */

define([
    'uiComponent'
], function (Component) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Lovebonito_Deliverytime/checkout/shipping/deliverytime'
        },
        getRules: function(){
            return {
                'delivery_time': {
                    'required': true
                }
            }
        }

    });

});