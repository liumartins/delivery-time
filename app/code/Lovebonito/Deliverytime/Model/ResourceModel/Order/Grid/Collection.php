<?php

/**
 * Created by PhpStorm.
 * User: lindomar
 * Date: 29/06/18
 * Time: 12:23 PM
 */



namespace Lovebonito\Deliverytime\Model\ResourceModel\Order\Grid;
use Magento\Sales\Model\ResourceModel\Order\Grid\Collection as OriginalCollection;
use Lovebonito\Deliverytime\Helper\Data as Helper;
/**
 * Order grid extended collection
 */
class Collection extends OriginalCollection
{
    protected function _renderFiltersBefore()
    {
        $joinTable = $this->getTable('sales_order');
        $this->getSelect()->joinLeft($joinTable, 'main_table.entity_id = sales_order.entity_id', ['delivery_time']);
        parent::_renderFiltersBefore();
    }
}