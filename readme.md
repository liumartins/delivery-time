How to install Lovebonito Delivery Time Module:

- Extract the folder app into your magento base directory 
	Ex: /var/www/magento-installation/
- Ru the commands that follow below on your server.

	1 php bin/magento setup:upgrade
	2 php bin/magento setup:static-content:deploy


PS: Make sure about the files and directories permissions.Magento
    Team suggests to set the file permissions to 660, the directory ones to 770. 
    It's can be done in the following way:
	sudo find . -type d -exec chmod 770 {} \; && sudo find . -type f -exec chmod 660 {} \; && sudo chmod u+x bin/magento

Follow attached some images prints to check where the module needs to be showing up.

